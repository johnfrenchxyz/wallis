# Wallis Theme

The **Wallis** Theme is for [Grav CMS](http://github.com/getgrav/grav).  This README.md file should be modified to describe the features, installation, configuration, and general usage of this theme.

## Description

A custom theme for the house of Wallis project
